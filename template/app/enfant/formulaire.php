<h2>Ajouter un enfant</h2>

<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Nom'); ?>
    <?php echo $form->input('nom','text', $enfant->nom ?? '') ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('Prénom'); ?>
    <?php echo $form->input('prenom','text', $enfant->prenom ?? '') ?>
    <?php echo $form->error('prenom'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>