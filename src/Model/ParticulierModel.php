<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ParticulierModel extends AbstractModel
{
    protected static $table = 'particulier';
    protected $nom;
    protected $prenom;
    protected $utilisateur_id;

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getUtilisateurId()
    {
        return $this->utilisateur_id;
    }

    /**
     * @param mixed $utilisateur_id
     */
    public function setUtilisateurId($utilisateur_id): void
    {
        $this->utilisateur_id = $utilisateur_id;
    }


    public static function insert($nom, $prenom, $utilisateur_id)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, prenom, utilisateur_id) VALUES (?,?,?)",
            array($nom, $prenom, $utilisateur_id)
        );
    }
}