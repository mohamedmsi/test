<h2>Enfant</h2>

<p><a class="btn" href="<?php echo $view->path('ajouter-enfant'); ?>">Ajouter un enfant</a></p>

<?php foreach ($enfants as $enfant) {
    echo '<div><h2>'.$enfant->nom . ' ' . $enfant->prenom.'</h2>
        <a class="btn" href="' .$view->path('enfant', array('id' => $enfant->id)).'">Information</a>
        <a class="btn" href="' .$view->path('modifier-enfant', array('id' => $enfant->id)).'">Modifier</a>
        <a class="btn" onclick="return confirm(\'Voulez-vous effacer ?\')" href="' .$view->path('supprimer-enfant', array('id' => $enfant->id)).'">Supprimer</a>
</div>';
}