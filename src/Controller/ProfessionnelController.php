<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Model\ProfessionnelModel;
use App\Service\Form;
use App\Service\SecurityService;
use App\Service\Validation;

/**
 *
 */
class ProfessionnelController extends BaseController
{
    public function professionnel()
    {

        if (empty($_SESSION['user']) || $_SESSION['user']['role'] !== 'professionnel') {
            $this->redirect('home');
        }
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors['nom'] = $v->textValid($post['nom'], 'Nom', 3, 50);
            $errors['prenom'] = $v->textValid($post['prenom'], 'Prénom', 3, 50);
            if ($v->isValid($errors)) {
                $nom = $post['nom'];
                $prenom = $post['prenom'];
                $utilisateur_id = $_SESSION['user']['id'];
                ProfessionnelModel::insert($nom, $prenom, $utilisateur_id);
                $this->addFlash('success', 'Merci d\'avoir complété votre inscription');
                $this->redirect('home');
            }
        }
        $form = new Form($errors);
        $this->render('app.professionnel.professionnel',array(
            'form' => $form
        ),'professionnel');
    }
}