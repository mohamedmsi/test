<?php

$routes = array(
    array(['GET'],'home','default','index'),

    array(['GET','POST'], 'login','security','login'),
    array(['GET','POST'], 'register','security','register'),
    array(['GET'], 'logout','security','logout'),

    array(['GET'], 'home_particulier','particulier','index'),
    array(['GET'], 'compte','particulier','compte'),

    array(['GET','POST'], 'particulier','particulier','particulier'),
    array(['GET','POST'], 'compte','particulier','compte'),
    array(['GET','POST'], 'information_particulier','particulier','detail', array('id')),
    array(['GET','POST'], 'detail','particulier','detail', array('id')),
    array(['GET','POST'], 'modifier-particulier','particulier','modifier', array('id')),
    array(['GET','POST'], 'supprimer-particulier','particulier','supprimer', array('id')),

    array(['GET','POST'], 'professionnel','professionnel','professionnel'),
);









