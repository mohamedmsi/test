<h2>Compléter votre compte particulier</h2>

<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Nom'); ?>
    <?php echo $form->input('nom','text', $particulier->nom ?? '') ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('Prénom'); ?>
    <?php echo $form->input('prenom','text', $particulier->prenom ?? '') ?>
    <?php echo $form->error('prenom'); ?>

    <?php echo $form->submit('submitted', 'Valider'); ?>
</form>