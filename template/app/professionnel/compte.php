<h2>Mon compte professionnel</h2>

<?php foreach ($professionnels as $professionnel) {
    echo '<div><h2>'.$professionnel->nom . ' ' . $professionnel->prenom.'</h2>
        <a class="btn" href="' .$view->path('professionnel', array('id' => $professionnel->id)).'">Information</a>
        <a class="btn" href="' .$view->path('modifier-professionnel', array('id' => $professionnel->id)).'">Modifier</a>
        <a class="btn" onclick="return confirm(\'Voulez-vous supprimer votre compte ?\')" href="' .$view->path('supprimer-professionnel', array('id' => $professionnel->id)).'">Supprimer</a>
</div>';
}