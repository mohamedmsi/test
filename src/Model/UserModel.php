<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected static $table = 'utilisateur';

    public static function getUserByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE email = ?",array($email),get_called_class(), true);
    }


    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (email, mot_de_passe, role) VALUES (?,?,?)",
            array($post['email'], $post['password_hash'], $post['role'])
        );
    }

    public static function getByUtilisateurId($utilisateur_id)
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE utilisateur_id = ?",array($utilisateur_id),get_called_class());
    }
}