<?php

namespace App\Controller;

use App\Service\SecurityService;
use Core\Kernel\AbstractController;

class BaseController extends AbstractController
{
    public function isLogged() {
        $security = new SecurityService();
        return $security->isLogged();
    }
}