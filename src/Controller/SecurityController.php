<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\SecurityService;
use App\Service\Validation;

/**
 *
 */
class SecurityController extends BaseController
{
    public function register()
    {

        if($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors['email'] = $v->emailValid($post['email']);
            $errors['mot_de_passe'] = $v->validPassword($post['mot_de_passe'], $post['confirmation_mot_de_passe']);
            if($v->isValid($errors)) {
                $verifUser = UserModel::getUserByEmail($post['email']);
                if(!empty($verifUser)) {
                    $errors['email'] = 'Votre compte existe dèjà';
                }
                if ($v->isValid($errors)) {
                    $post['password_hash'] = password_hash($post['mot_de_passe'], PASSWORD_DEFAULT);
                    UserModel::insert($post);
                    $this->addFlash('success', 'Merci pour votre inscription');
                    $this->redirect('login');
                }
            }
        }
        $form = new Form($errors);
        $this->render('app.security.register',array(
            'form' => $form
        ));
    }

    public function login()
    {
        if($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $user = UserModel::getUserByEmail($post['email']);
            if(empty($user)) {
                $errors['email'] = 'Error Credentials';
            } else {
                if(!password_verify($post['mot_de_passe'], $user->mot_de_passe)) {
                    $errors['email'] = 'Error Credentials';
                } else {
                    // creation de la session
                    $_SESSION['user'] = array(
                        'id'     => $user->id,
                        'email'  => $user->email,
                        'role'  => $user->role,
                        'ip'     => $_SERVER['REMOTE_ADDR']
                    );
                    $this->FormulaireACompleter();
                    $this->addFlash('success', 'Nous sommes heureux de vous revoir');
                    $this->redirect('home');
                }
            }
        }
        $form = new Form($errors);
        $this->render('app.security.login',array(
            'form' => $form
        ));
    }

    protected function FormulaireACompleter()
    {
        $user = UserModel::getByUtilisateurId($_SESSION['user']['id']);

        if (!$user->profil_complet) {
            if ($_SESSION['user']['role'] === 'particulier') {
                $this->redirect('particulier');
            } elseif ($_SESSION['user']['role'] === 'professionnel') {
                $this->redirect('professionnel');
            }
        }
    }

    public function logout()
    {
        $_SESSION = array();
        session_destroy();
        $this->redirect('home');
    }
}