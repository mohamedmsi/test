<h2>Connexion</h2>

<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Adresse e-mail'); ?>
    <?php echo $form->input('email','email') ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->label('Mot de passe'); ?>
    <?php echo $form->input('mot_de_passe','password') ?>
    <?php echo $form->error('mot_de_passe'); ?>

    <?php echo $form->submit('submitted', 'Connexion'); ?>
</form>