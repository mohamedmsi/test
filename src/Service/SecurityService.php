<?php
namespace App\Service;
/**
 *  Security Helper
 */
class SecurityService
{
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function isLogged() {
        if(!empty($_SESSION['user']['id'])) {
            if(!empty($_SESSION['user']['email'])) {
                if(!empty($_SESSION['user']['ip'])) {
                    if($_SESSION['user']['ip'] == $_SERVER['REMOTE_ADDR']) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}