<h2>Compléter votre compte professionnel</h2>

<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Nom'); ?>
    <?php echo $form->input('nom','text', $professionnel->nom ?? '') ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('Prénom'); ?>
    <?php echo $form->input('prenom','text', $professionnel->prenom ?? '') ?>
    <?php echo $form->error('prenom'); ?>

    <?php echo $form->submit('submitted', 'Valider'); ?>
</form>