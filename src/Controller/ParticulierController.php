<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Model\ProfessionnelModel;
use App\Model\ParticulierModel;
use App\Service\Form;
use App\Service\SecurityService;
use App\Service\Validation;

/**
 *
 */
class ParticulierController extends BaseController
{
    public function particulier()
    {

        if (empty($_SESSION['user']) || $_SESSION['user']['role'] !== 'particulier') {
            $this->redirect('home');
        }
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors['nom'] = $v->textValid($post['nom'], 'Nom', 3, 50);
            $errors['prenom'] = $v->textValid($post['prenom'], 'Prénom', 3, 50);
            if ($v->isValid($errors)) {
                $nom = $post['nom'];
                $prenom = $post['prenom'];
                $utilisateur_id = $_SESSION['user']['id'];
                ParticulierModel::insert($nom, $prenom, $utilisateur_id);
                $this->addFlash('success', 'Merci d\'avoir complété votre inscription');
                $this->redirect('home_particulier');
            }
        }
        $form = new Form($errors);
        $this->render('app.particulier.particulier',array(
            'form' => $form
        ),'particulier');
    }

    public function index()
    {
        $professionnels = ProfessionnelModel::getAllProfessionnels();
        $this->render('app.particulier.index',array(
            'professionnels' => $professionnels
        ), 'particulier');
    }

    public function compte()
    {
        $this->render('app.particulier.compte', array(
        ), 'particulier');
    }
}